package net.mitogen.swear;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import android.app.Activity;
import android.graphics.Typeface;

import com.loopj.android.http.*;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class startActivity extends Activity {
	
	private LinearLayout wordList;
	private Button button;
	private EditText editText;
	private AsyncHttpClient client = new AsyncHttpClient();
	private AsyncHttpResponseHandler responseHandler;
	
	private TextView title1;
	private TextView title2;
	
	private Typeface typeface;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        title1 = (TextView)findViewById(R.id.title1);
        typeface = Typeface.createFromAsset(getAssets(),"rolina.ttf");
        title1.setTypeface(typeface);
        
        title2 = (TextView)findViewById(R.id.title2);
        title2.setTypeface(typeface);
        
        wordList = (LinearLayout)findViewById(R.id.linearLayout1);
        editText = (EditText) findViewById(R.id.editText1);
        button = (Button)findViewById(R.id.button1);
        
        //editText.getText().append("sok"); //TODO REMOVE
        
        button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String word = editText.getText().toString();
				
				if (word.length() == 0) {
					wordList.removeAllViews();
					populateList(wordList, "empty");
					return;
				}
				else {
					try {
						String phrase = URLEncoder.encode(word, "UTF-8");
						client.get("http://swearmaster2.appspot.com/search?phrase=" + phrase.toLowerCase(), responseHandler);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
					
				AlphaAnimation aa = new AlphaAnimation(1, 0); //ZNIKANIE
        		aa.setDuration(500);
        		aa.setFillAfter(true);
        		aa.setAnimationListener(new AnimationListener() {
					public void onAnimationStart(Animation animation) {}
					public void onAnimationRepeat(Animation animation) {}
					public void onAnimationEnd(Animation animation) {
						wordList.removeAllViews();
					}
				});
        		wordList.startAnimation(aa);
				
			}
		});
        
        responseHandler = new AsyncHttpResponseHandler() {
        	@Override
            public void onSuccess(String response) {
        		wordList.clearAnimation();
        		populateList(wordList, response);
        		
        		AlphaAnimation aa = new AlphaAnimation(0, 1);
        		aa.setDuration(500);
        		aa.setFillAfter(true);
        		wordList.startAnimation(aa);
            }
        	
        	@Override
        	public void onFailure(Throwable e) {
        		wordList.clearAnimation();
        		populateList(wordList, "fail");
        		
        		AlphaAnimation aa = new AlphaAnimation(0, 1);
        		aa.setDuration(500);
        		aa.setFillAfter(true);
        		wordList.startAnimation(aa);
        	}
        };

    }
    
    
    void populateList(LinearLayout wordList, String response) {
    	
    	if (response.equals("fail")) {
    		TextView nope = new TextView(startActivity.this);
			nope.setTypeface(typeface);
			nope.setTextSize(25);
			nope.setText("No way!");
			nope.setGravity(Gravity.CENTER_HORIZONTAL);
			wordList.addView(nope);
			
			TextView nope2 = new TextView(startActivity.this);
			nope2.setTypeface(typeface);
			nope2.setTextSize(20);
			nope2.setText("We will need an internet connection for that, sir.");
			nope2.setGravity(Gravity.CENTER_HORIZONTAL);
			wordList.addView(nope2);
    		return;
    	}
    	
    	if (response.equals("empty")) {
    		TextView nope = new TextView(startActivity.this);
			nope.setTypeface(typeface);
			nope.setTextSize(25);
			nope.setText("No way!");
			nope.setGravity(Gravity.CENTER_HORIZONTAL);
			wordList.addView(nope);
			
			TextView nope2 = new TextView(startActivity.this);
			nope2.setTypeface(typeface);
			nope2.setTextSize(20);
			nope2.setText("You will have to put something there, sir.");
			nope2.setGravity(Gravity.CENTER_HORIZONTAL);
			wordList.addView(nope2);
    		return;
    	}
    	
    	String lines[] = response.split("\\r?\\n");
		for(String line : lines) {
			String args[] = line.split("#");
			
			if (args.length != 3) {
				TextView nope = new TextView(startActivity.this);
				nope.setTypeface(typeface);
				nope.setTextSize(25);
				nope.setText("Nope.");
				nope.setGravity(Gravity.CENTER_HORIZONTAL);
    			wordList.addView(nope);
    			
    			TextView nope2 = new TextView(startActivity.this);
				nope2.setTypeface(typeface);
				nope2.setTextSize(20);
				nope2.setText("It means what it means. Obviously.");
				nope2.setGravity(Gravity.CENTER_HORIZONTAL);
    			wordList.addView(nope2);
    			
				break;
			}
				
			
			TextView swear = new TextView(startActivity.this);
			swear.setTypeface(typeface);
			swear.setTextSize(25);
			swear.setText(args[0]);
			swear.setGravity(Gravity.CENTER_HORIZONTAL);
			wordList.addView(swear);
			
			TextView t1 = new TextView(startActivity.this);
			t1.setTypeface(typeface);
			t1.setText("means");
			t1.setTextSize(20);
			t1.setGravity(Gravity.CENTER_HORIZONTAL);
			wordList.addView(t1);
			
			TextView meaning = new TextView(startActivity.this);
			meaning.setTypeface(typeface);
			meaning.setText(args[1]);
			meaning.setTextSize(25);
			meaning.setGravity(Gravity.CENTER_HORIZONTAL);
			wordList.addView(meaning);
			
			TextView language = new TextView(startActivity.this);
			language.setTypeface(typeface);
			language.setText("in " + args[2]);
			language.setTextSize(20);
			language.setGravity(Gravity.CENTER_HORIZONTAL);
			wordList.addView(language);
			
			TextView margin = new TextView(startActivity.this);
			margin.setText(" ");
			margin.setTextSize(20);
			wordList.addView(margin);
        	
		}
		
       
    }
    

}