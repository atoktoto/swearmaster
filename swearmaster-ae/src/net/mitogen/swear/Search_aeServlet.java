package net.mitogen.swear;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.*;

import java.util.logging.Logger;

import com.google.appengine.api.datastore.DatastoreNeedIndexException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.DatastoreTimeoutException;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;


@SuppressWarnings("serial")
public class Search_aeServlet extends HttpServlet {
	
	private List<Entity> search(String queryString, DatastoreService ds) {
	     Query q = new Query("swears");
	     q.addFilter("words", FilterOperator.EQUAL, queryString);
	     List<Entity> results = ds.prepare(q).asList(FetchOptions.Builder.withDefaults());
	     return results;
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String phrase = req.getParameter("phrase");
		phrase = URLDecoder.decode(phrase, "UTF-8");
		
		resp.setContentType("text/plain");
		//resp.getWriter().println(phrase);
		//resp.getWriter().println("#");
		//resp.getWriter().println("Result:<br/>");
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		List<Entity> results = search(phrase, datastore);
		
		for (Entity e: results) {
			resp.getWriter().println(e.getProperty("swear") + "#" + e.getProperty("meaning") + "#" + e.getProperty("language"));
		}
		
	}
}