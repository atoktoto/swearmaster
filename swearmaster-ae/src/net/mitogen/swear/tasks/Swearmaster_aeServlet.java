package net.mitogen.swear.tasks;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Vector;

import javax.servlet.http.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

@SuppressWarnings("serial")
public class Swearmaster_aeServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doPost(req, resp);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		
		String link = req.getParameter("url");
		link = URLDecoder.decode(link, "UTF-8");
		resp.getWriter().println(link);
		
		Document docLang = Jsoup.connect(link).get();
		Elements tableRows = docLang.select(".padding10 table tr");
		tableRows.remove(0);
		tableRows.remove(0);
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Vector<Entity> vect = new Vector<Entity>();
		
		for (Element row : tableRows ) {
			try {
				Elements words = row.select("td");
				String swear = words.get(0).text();
				String meaning =  words.get(1).text();
				String language = link.substring(link.indexOf("=") + 1);
				language = URLDecoder.decode(language, "UTF-8");
				resp.getWriter().println(swear + " - - - " + meaning);
				
				Entity e = new Entity("swears");
		        e.setProperty("swear", swear);
		        HashSet<String> w = new HashSet<String>(10);
		        for (String s: swear.split(" ")) {
		        	s = s.toLowerCase();
		        	s = s.replaceAll("('|!|\\?|\")", "");
		        	w.add(s);
		        }
		        e.setProperty("words", w);
		        e.setProperty("meaning", meaning);
		        e.setProperty("source", link);
		        e.setProperty("language", language);
		        vect.add(e);
			} catch (Exception e) {
			}
		}
		
		datastore.put(vect);
	}
}
