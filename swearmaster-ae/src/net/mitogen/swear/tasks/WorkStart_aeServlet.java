package net.mitogen.swear.tasks;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.http.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import static com.google.appengine.api.taskqueue.TaskOptions.Builder.*;

@SuppressWarnings("serial")
public class WorkStart_aeServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		Document doc = Jsoup.connect("http://www.youswear.com/").get();
		Elements newsHeadlines = doc.select(".list .padding10 a");
		
		resp.setContentType("text/plain");
		
		Queue queue = QueueFactory.getDefaultQueue();
		
		for (Element headline : newsHeadlines ) {
			String link = headline.attr("href");
			String site = "www.youswear.com";
			
			link = "http://" + site + "/" + link.substring(3);
			
			link = URLEncoder.encode(link, "UTF-8");
			
			
		    queue.add(withUrl("/parse_site1").param("url", link));
			
			resp.getWriter().println(headline.text());
			resp.getWriter().println(link);
		}
		
		
	}
}
